package gostack

import (
	"fmt"
	"testing"
)

var (
	testStack Stack
)

func TestLen(t *testing.T) {

	if s := testStack.Len(); s != 0 {
		t.Fail()
	}

	testStack.size++
	if s := testStack.Len(); s != 1 {
		t.Fail()
	}
}

func TestPush(t *testing.T) {
	testStack := new(Stack)
	a, b, c := 1, 2, 3

	testStack.Push(a)
	fmt.Printf("testStack.size = %v\n", testStack.size)
	testStack.Push(b)
	fmt.Printf("testStack.size = %v\n", testStack.size)
	testStack.Push(c)
	fmt.Printf("testStack.size = %v\n", testStack.size)

	if testStack.size != 3 {
		t.Fail()
	}
}

func TestPop(t *testing.T) {

	fmt.Printf("testStack: %v\n", testStack.elements)

	testStack := new(Stack)
	a, b, c := 1, 2, 3
	testStack.Push(a)
	testStack.Push(b)
	testStack.Push(c)

	d := testStack.Pop()
	fmt.Printf("Value popped = %v\n", d)
	if d != 3 {
		t.Fail()
	}

	if testStack.size != 2 {
		t.Fail()
	}

}

func TestReverse(t *testing.T) {

	testStack := new(Stack)
	a, b, c := 1, 2, 3
	testStack.Push(a)
	testStack.Push(b)
	testStack.Push(c)

	testStack.Reverse()

	if v := testStack.Pop(); v != a {
		t.Fail()
	}
}
