// gostack project gostack.go
package gostack

type Stack struct {
	elements []interface{}
	size     int
}

func (s *Stack) Len() int {
	return s.size
}

func (s *Stack) Push(val interface{}) {
	s.elements = append(s.elements, val)
	s.size++
}

func (s *Stack) Pop() (val interface{}) {
	val, s.elements = s.elements[len(s.elements)-1], s.elements[:len(s.elements)-1]
	s.size--
	return
}

func (s *Stack) Peek() (val interface{}) {
	return s.elements[len(s.elements)-1]
}

func (s *Stack) Reverse() {

	if s.size == 0 {
		return
	}

	for i := len(s.elements)/2 - 1; i >= 0; i-- {
		e := len(s.elements) - 1 - i
		s.elements[i], s.elements[e] = s.elements[e], s.elements[i]
	}
}
